#include "FpsTester.h"

FpsTester::FpsTester()
{
	m_MaxCount = 30;

	Reset();
}

FpsTester::~FpsTester()
{

}

void FpsTester::Reset()
{
	m_Times.clear();
	m_bGotFirstFrame = false;
}

void FpsTester::Frame()
{
	if(!m_bGotFirstFrame)
	{
		m_bGotFirstFrame = true;
	}
	else
	{
		m_Times.push_back(GetTickCount() - m_OldTime);
		while(m_Times.size() > m_MaxCount)
		{
			m_Times.pop_front();
		}
	}

	m_OldTime = GetTickCount();
}

float FpsTester::GetFps()
{
	if(m_Times.size() == 0) return 0;

	float total = 0;
	for(int i = 0; i < m_Times.size(); i++)
	{
		total += m_Times[i];
	}

	return m_Times.size() / (total / 1000);
}

void FpsTester::SetSampleCount(int count)
{
	m_MaxCount = count;
}
