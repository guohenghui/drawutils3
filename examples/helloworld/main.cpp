#include <stdio.h>
#include "drawutils3.h"

// include stl
#include <string>
#include <iostream>
#include <vector>

// include opencv
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#define TEST_IMG "../../../data/WaterLilies.jpg"

static int g_CanvasType = DU_CANVAS_GDI;
static int g_DrawMode = DUDRAW_ZOOM;
void TestBGR()
{
	char buf[MAX_PATH];
	sprintf(buf, "drawutil-%d", 0);
	std::string wndName = buf;
	cv::namedWindow(wndName.c_str(), cv::WINDOW_NORMAL);
	HWND hWnd = (HWND)cvGetWindowHandle(wndName.c_str());

	cv::Mat img = cv::imread(TEST_IMG);

	void* pCanv = DUCreateCanvas(g_CanvasType, hWnd, DUPIX_BGR, img.cols, img.rows, img.step, g_DrawMode);
	if(pCanv)
	{
		while(true)
		{
			DUImage duImg;
			memset(&duImg, 0, sizeof(duImg));

			duImg.nPlaneCount = 1;
			duImg.pDatas[0] = img.data;
			duImg.nStrides[0] = img.step;
			DUSetROI(pCanv, 100, 100, 500, 500);
			DUSetPadding(pCanv, 10, 10, 10, 10);
			DUBeginPaint2(pCanv, &duImg);

			DUEndPaint(pCanv);

			if(cv::waitKey(40) == VK_ESCAPE)
				break;
		}
	}
	else
	{
		printf("创建画布失败\n");
	}

	DUDeleteCanvas(pCanv);
}

void TestBGRA()
{
	char buf[MAX_PATH];
	sprintf(buf, "drawutil-%d", 0);
	std::string wndName = buf;
	cv::namedWindow(wndName.c_str(), cv::WINDOW_NORMAL);
	HWND hWnd = (HWND)cvGetWindowHandle(wndName.c_str());

	cv::Mat img0 = cv::imread(TEST_IMG);
	cv::Mat img;
	cv::cvtColor(img0, img, CV_BGR2BGRA);

	void* pCanv = DUCreateCanvas(g_CanvasType, hWnd, DUPIX_BGRA, img.cols, img.rows, img.step, g_DrawMode);
	if(pCanv)
	{
		while(true)
		{
			DUImage duImg;
			memset(&duImg, 0, sizeof(duImg));

			duImg.nPlaneCount = 1;
			duImg.pDatas[0] = img.data;
			duImg.nStrides[0] = img.step;
			DUSetROI(pCanv, 100, 100, 500, 500);
			DUSetPadding(pCanv, 10, 10, 10, 10);
			DUBeginPaint2(pCanv, &duImg);

			DUEndPaint(pCanv);

			if(cv::waitKey(40) == VK_ESCAPE)
				break;
		}
	}
	else
	{
		printf("创建画布失败\n");
	}

	DUDeleteCanvas(pCanv);
}

void TestI420()
{
	char buf[MAX_PATH];
	sprintf(buf, "drawutil-%d", 0);
	std::string wndName = buf;
	cv::namedWindow(wndName.c_str(), cv::WINDOW_NORMAL);
	HWND hWnd = (HWND)cvGetWindowHandle(wndName.c_str());

	cv::Mat img0 = cv::imread(TEST_IMG);
	cv::Mat img;
	cv::cvtColor(img0, img, CV_BGR2YUV_I420);

	void* pCanv = DUCreateCanvas(g_CanvasType, hWnd, DUPIX_I420, img0.cols, img0.rows, img.step, g_DrawMode);
	if(pCanv)
	{
		while(true)
		{
			DUImage duImg;
			memset(&duImg, 0, sizeof(duImg));

			duImg.nPlaneCount = 1;
			duImg.pDatas[0] = img.data;
			duImg.nStrides[0] = img.step;
			DUSetROI(pCanv, 0, 0, 500, 500);
			DUSetPadding(pCanv, 10, 10, 10, 10);
			DUBeginPaint2(pCanv, &duImg);

			DUEndPaint(pCanv);

			if(cv::waitKey(40) == VK_ESCAPE)
				break;
		}
	}
	else
	{
		printf("创建画布失败\n");
	}

	DUDeleteCanvas(pCanv);
}

void TestI420_2()
{
	char buf[MAX_PATH];
	sprintf(buf, "drawutil-%d", 0);
	std::string wndName = buf;
	cv::namedWindow(wndName.c_str(), cv::WINDOW_NORMAL);
	HWND hWnd = (HWND)cvGetWindowHandle(wndName.c_str());

	cv::Mat img0 = cv::imread(TEST_IMG);
	cv::Mat img;
	cv::cvtColor(img0, img, CV_BGR2YUV_I420);

	void* pCanv = DUCreateCanvas(g_CanvasType, hWnd, DUPIX_I420, img0.cols, img0.rows, img.step, g_DrawMode);
	if(pCanv)
	{
		while(true)
		{
			DUImage duImg;
			memset(&duImg, 0, sizeof(duImg));

			duImg.nPlaneCount = 3;
			duImg.pDatas[0] = img.data;
			duImg.pDatas[1] = img.data + img0.rows * img.step;
			duImg.pDatas[2] = duImg.pDatas[1] + img0.rows / 4 * img.step;
			duImg.nStrides[0] = img.step;
			duImg.nStrides[1] = img.step / 2;
			duImg.nStrides[2] = img.step / 2;
			DUSetROI(pCanv, 0, 0, 500, 500);
			DUSetPadding(pCanv, 0, 0, 0, 0);
			DUBeginPaint2(pCanv, &duImg);

			DUEndPaint(pCanv);

			if(cv::waitKey(40) == VK_ESCAPE)
				break;
		}
	}
	else
	{
		printf("创建画布失败\n");
	}

	DUDeleteCanvas(pCanv);
}

int main(int argc, char* argv[])
{
	// TestBGR();
	// TestBGRA();
	TestI420();
	// TestI420_2();
	return 0;
}

