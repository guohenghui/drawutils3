#pragma once

#include <Windows.h>

#include <deque>

class FpsTester
{
public:
	FpsTester();
	~FpsTester();

	void Reset();

	void Frame();

	float GetFps();

	void SetSampleCount(int count);
private:
	bool m_bGotFirstFrame;
	int m_MaxCount;
	long m_OldTime;
	std::deque<long> m_Times;
};