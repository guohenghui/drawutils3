#include "stdafx.h"
#include "GDICanvas.h"
#include "DXSurfaceMgr.h"
#include <assert.h>
#include <dxbox.h>

DRAWUTIL_NS_BEGIN

CGDICanvas::CGDICanvas(void)
{
	Init();
}

CGDICanvas::~CGDICanvas(void)
{
	Release();
}

void CGDICanvas::Init()
{
	ICanvas::Init();

	m_hFrontDC = NULL;
	m_hOldBmp = NULL;
	m_hBmp = NULL;
	m_pData = NULL;

	m_bCreated = false;
}

void CGDICanvas::Reset()
{
	if(m_hFrontDC != NULL) ::ReleaseDC(m_hWnd, m_hFrontDC);
	if(m_hOldBmp != NULL) ::SelectObject(m_hDC, m_hOldBmp);
	if(m_hBmp != NULL) ::DeleteObject(m_hBmp);
	if(m_hDC != NULL) ::DeleteDC(m_hDC);
	if(m_pData != NULL) delete[] m_pData;

	Init();
}

bool CGDICanvas::Create( HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode )
{
	bool bSucceed = false;

	if(!m_bCreated)
	{
		ICanvas::Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode);

		do {
			m_hFrontDC = ::GetDC(m_hWnd);
			if(m_hFrontDC == NULL) break;

			m_hDC = ::CreateCompatibleDC(m_hFrontDC);
			if(m_hDC == NULL) break;

			//m_hBmp = ::CreateBitmap(nWidth, nHeight, 1, 32, NULL);
			m_hBmp = ::CreateCompatibleBitmap(m_hFrontDC, nWidth, nHeight);
			if(m_hBmp == NULL) break;

			m_hOldBmp = (HBITMAP)::SelectObject(m_hDC, m_hBmp);
			m_pData = new BYTE[nWidth * nHeight * 4];
			if(m_pData == NULL) break;

			SetStretchBltMode(m_hFrontDC, /*HALFTONE*//*COLORONCOLOR*/COLORONCOLOR);
			SetStretchBltMode(m_hDC, /*HALFTONE*//*COLORONCOLOR*/COLORONCOLOR);

			bSucceed = true;
		} while (false);
		

		if(!bSucceed)
		{
			Reset();
		}
	}

	m_bCreated = bSucceed;
	
	return bSucceed;
}

void CGDICanvas::Release()
{
	Reset();
}
void CGDICanvas::fillBmpInfo(BITMAPINFO* pBMI, int width, int height, int bpp, int origin)
{
	assert( pBMI && width >= 0 && height >= 0 && (bpp == 8 || bpp == 24 || bpp == 32));

	BITMAPINFOHEADER* bmih = &(pBMI->bmiHeader);

	memset( bmih, 0, sizeof(*bmih));
	bmih->biSize = sizeof(BITMAPINFOHEADER);
	bmih->biWidth = width;
	if(height < 0) height = -height;
	bmih->biHeight = origin ? height : -height;
	bmih->biPlanes = 1;
	bmih->biBitCount = (unsigned short)bpp;
	bmih->biCompression = BI_RGB;

	if( bpp == 8 )
	{
		RGBQUAD* palette = pBMI->bmiColors;
		int i;
		for( i = 0; i < 256; i++ )
		{
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}
}

void CGDICanvas::BeginPaint(BYTE* pData)
{
// 	if(pDC)
// 	{
// 		*pDC = m_hDC;
// 	}
	
	if(pData)
	{
		FillData(pData);
		////		int pixFmt = m_nPixelFormat;
		//// 		if(pixFmt == DUPIX_RGB || pixFmt == DUPIX_RGBA || DUPIX_ARGB
		//// 			|| pixFmt == DUPIX_BGR || pixFmt == DUPIX_BGRA || DUPIX_ABGR)
		//		{
		//			SetBitmapBits(m_hBmp, m_nWidth * m_nHeight * 4, m_pData);
		//		}

		BYTE buffer[sizeof(BITMAPINFOHEADER) + 1024];
		BITMAPINFO* bmi = (BITMAPINFO*)buffer;
		fillBmpInfo( bmi, m_nWidth, m_nHeight, 32, 0);

		if(::StretchDIBits(
			m_hDC,
			0, 0, m_nWidth, m_nHeight,
			0, 0, m_nWidth, m_nHeight,
			m_pData, bmi, DIB_RGB_COLORS, SRCCOPY ) == GDI_ERROR)
		{
			// printf("GDI_ERROR\n");
		}
	}
}

void CGDICanvas::BeginPaint(DUImage* pImage)
{
	switch(m_nPixelFormat)
	{
	case DUPIX_BGR:
		FillData(pImage->pDatas[0]);
		break;
	case DUPIX_BGRA:
		FillData(pImage->pDatas[0]);
		break;
	case DUPIX_I420:
	case DUPIX_YV12:
		{
			MCoreCvtImageInfo info;
			memset(&info, 0, sizeof(info));
			info.width = m_nWidth;
			info.height = m_nHeight;
			info.input.csp = (m_nPixelFormat == DUPIX_I420) ? MCORE_CSP_I420 : MCORE_CSP_YV12;
			for(int i = 0; i < pImage->nPlaneCount; i++) {
				info.input.plane[i] = pImage->pDatas[i];
				info.input.stride[i] = pImage->nStrides[i];
			}
			info.output.csp = MCORE_CSP_BGRA;
			info.output.plane[0] = m_pData;
			info.output.stride[0] = m_nWidth * 4;

			int errCode = 0;
			errCode = MCoreCvtImage(&info);
		}
		break;
	}

	BYTE buffer[sizeof(BITMAPINFOHEADER) + 1024];
	BITMAPINFO* bmi = (BITMAPINFO*)buffer;
	fillBmpInfo( bmi, m_nWidth, m_nHeight, 32, 0);

	if(::StretchDIBits(
		m_hDC,
		0, 0, m_nWidth, m_nHeight,
		0, 0, m_nWidth, m_nHeight,
		m_pData, bmi, DIB_RGB_COLORS, SRCCOPY ) == GDI_ERROR)
	{
		// printf("GDI_ERROR\n");
	}
}

void CGDICanvas::EndPaint()
{
	Refresh();

	ICanvas::EndPaint();
}

void CGDICanvas::Refresh()
{
	if(!m_hDC) return;

	XRect drawRect;
	GetClientRect(m_hWnd, &drawRect);
	// Do padding
	drawRect.left += m_Padding.left;
	drawRect.top += m_Padding.top;
	drawRect.right -= m_Padding.right;
	drawRect.bottom -= m_Padding.bottom;
	if(drawRect.right <= drawRect.left || drawRect.bottom <= drawRect.top)
		return;

	int width = m_nWidth;
	int height = m_nHeight;
	if(m_nDrawMode == DUDRAW_NORMAL)
	{
		int w = std::min(width, drawRect.Width());
		int h = std::min(height, drawRect.Height());
		if(w >= 0 && height >= 0)
		{
			XRect srcRect = XRect(0, 0, w, h);
			XRect dstRect = XRect(drawRect.left, drawRect.top, drawRect.left + w, drawRect.top + h);
			::StretchBlt(m_hFrontDC, dstRect.left, dstRect.top, drawRect.Width(), drawRect.Height(), m_hDC, srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height(), SRCCOPY);
		}
	}
	else if(m_nDrawMode == DUDRAW_CENTER)
	{
		int w = std::min(width, drawRect.Width());
		int h = std::min(height, drawRect.Height());
		if(w >= 0 && height >= 0)
		{
			XRect srcRect = XRect(width / 2 - w / 2, height / 2 - h / 2, width / 2 + w / 2, height / 2 + h / 2);
			POINT cenPoint = {(drawRect.left + drawRect.right) / 2, (drawRect.top + drawRect.bottom) / 2 };
			XRect dstRect = XRect(cenPoint.x - w / 2, cenPoint.y - h / 2, cenPoint.x + w / 2, cenPoint.y + h / 2);

			::StretchBlt(m_hFrontDC, dstRect.left, dstRect.top, drawRect.Width(), drawRect.Height(), m_hDC, srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height(), SRCCOPY);
		}
	}
	else if(m_nDrawMode == DUDRAW_STRECH)
	{
		::StretchBlt(m_hFrontDC, drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height(), m_hDC, 0, 0, m_nWidth, m_nHeight, SRCCOPY);
	}
	else if(m_nDrawMode == DUDRAW_ZOOM)
	{
		GetZoomedInnerWindow(&drawRect.left,&drawRect.top,&drawRect.right,&drawRect.bottom,(LONG)width,(LONG)height);

		::StretchBlt(m_hFrontDC, drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height(), m_hDC, 0, 0, m_nWidth, m_nHeight, SRCCOPY);
	}
	else if(m_nDrawMode == DUDRAW_AUTO)
	{
		int l, t, w, h;
		GetCanvasPaintRect(&l, &t, &w, &h);
		XRect srcRect(l, t, l + w, t + h);
		GetWindowPaintRect(&l, &t, &w, &h);
		XRect dstRect(l, t, l + w, t + h);

		::StretchBlt(m_hFrontDC, dstRect.left, dstRect.top, dstRect.Width(), dstRect.Height(), m_hDC, srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height(), SRCCOPY);
	}
}

void CGDICanvas::FillData( BYTE* pData )
{
	int pixFmt = m_nPixelFormat;
	if(pixFmt == DUPIX_I420 || pixFmt == DUPIX_YV12)
	{
		MCoreCvtImageInfo info;
		memset(&info, 0, sizeof(info));
		info.width = m_nWidth;
		info.height = m_nHeight;
		info.input.csp = (pixFmt == DUPIX_I420) ? MCORE_CSP_I420 : MCORE_CSP_YV12;
		info.input.plane[0] = pData;
		info.input.stride[0] = m_nWidth;
		info.output.csp = MCORE_CSP_BGRA;
		info.output.plane[0] = m_pData;
		info.output.stride[0] = m_nWidth * 4;

		int errCode = 0;
		errCode = MCoreCvtImage(&info);
	}
	else if(pixFmt == DUPIX_BGR || pixFmt == DUPIX_BGRA)
	{ 
		int srcPixelSize = ((pixFmt == DUPIX_BGR) ? 3 : 4); 
		int dstPixelSize = 4;
		int srcStride = m_nStride;
		int dstStride = m_nWidth * 4;
		int W = m_nWidth;
		int H = m_nHeight;
		if(pixFmt == DUPIX_BGR)
		{
			// #pragma omp parallel for
			for(int i = 0; i < H; i++)
			{
				LPBYTE pSrc = pData + i * srcStride;
				LPBYTE pDst = m_pData + i * dstStride;

				for (int j = 0; j < W; j++)
				{
					pDst[0] = pSrc[0];
					pDst[1] = pSrc[1];
					pDst[2] = pSrc[2];

					pSrc += srcPixelSize;
					pDst += dstPixelSize;
				}
			}
		}
		else if(pixFmt == DUPIX_BGRA)
		{
			// #pragma omp parallel for
			for(int i = 0; i < H; i++)
			{
				LPBYTE pSrc = pData + i * srcStride;
				LPBYTE pDst = m_pData + i * dstStride;

				for (int j = 0; j < W; j++)
				{
					pDst[0] = pSrc[0];
					pDst[1] = pSrc[1];
					pDst[2] = pSrc[2];
					pDst[3] = pSrc[3];

					pSrc += srcPixelSize;
					pDst += dstPixelSize;
				}
			}
		}

	}
}

DRAWUTIL_NS_END