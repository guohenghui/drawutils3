#include "stdafx.h"
#include "ICanvas.h"
#include "ZoomEngine.h"

DRAWUTIL_NS_BEGIN

void Throw(ErrorCode errCode)
{
	throw Exception(errCode);
}

void Throw(ErrorCode errCode, HRESULT hr)
{
	throw Exception(errCode, hr);
}

ICanvas::ICanvas(void)
{
	m_hWnd = NULL;
	m_nPixelFormat = 0;
	m_nWidth = 0;
	m_nStride = 0;
	m_nHeight = 0;
	m_nDrawMode = 0;
	m_hDC = NULL;
	m_hOldPen = NULL;
	m_bMouseScrollEnabled = FALSE;

	::SetRect(&m_Padding, 0, 0, 0, 0);
	::SetRect(&m_ROI, 0, 0, 0, 0);
}

ICanvas::~ICanvas(void)
{
}

bool ICanvas::Create( HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode )
{
	m_hWnd = hWnd;
	m_nPixelFormat = nPixelFormat;
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_nStride = nStride;
	m_nDrawMode = nDrawMode;
	m_hDC = NULL;

	m_ZoomEngine.SetCanvasSize(Size(nWidth, nHeight));
	//m_ZoomEngine.SetCanvasViewSize(1);

	return true;
}

void ICanvas::Release()
{

}

void ICanvas::SetDrawMode( int nDrawMode )
{
	m_nDrawMode = nDrawMode;
}

void ICanvas::SetPadding( int nLeft, int nTop, int nRight, int nBottom )
{
	m_Padding.left = nLeft;
	m_Padding.top = nTop;
	m_Padding.right = nRight;
	m_Padding.bottom = nBottom;
}

void ICanvas::SetROI( int nLeft, int nTop, int nRight, int nBottom  )
{
	m_ROI.left = nLeft;
	m_ROI.top = nTop;
	m_ROI.right = nRight;
	m_ROI.bottom = nBottom;
}

void ICanvas::BeginPaint(BYTE* pData)
{

}

void ICanvas::EndPaint()
{
	if(m_hOldPen != NULL)
	{
		HPEN hPen = (HPEN)::SelectObject(m_hDC, m_hOldPen);
		::DeleteObject(hPen);
	}
}

void ICanvas::Refresh()
{
}

HDC ICanvas::GetPaintingDC()
{
	return m_hDC;
}

void ICanvas::SetPen( int nSyle, int nWidth, COLORREF Clr )
{
	if(m_hDC != NULL)
	{
		if(m_hOldPen == NULL)
		{
			HPEN hPen = ::CreatePen(nSyle, nWidth, Clr);
			m_hOldPen = (HPEN)::SelectObject(m_hDC, hPen);
		}
		else
		{
			HPEN hPen = (HPEN)::SelectObject(m_hDC, m_hOldPen);
			::DeleteObject(hPen);

			hPen = ::CreatePen(nSyle, nWidth, Clr);
			m_hOldPen = (HPEN)::SelectObject(m_hDC, hPen);
		}
	}
}

void ICanvas::MoveTo( int x, int y )
{
	HDC hDC = GetPaintingDC();
	if(hDC)
	{
		::MoveToEx(hDC, x, y, NULL);
	}
}

void ICanvas::LineTo( int x, int y )
{
	HDC hDC = GetPaintingDC();
	if(hDC)
	{
		::LineTo(hDC, x, y);
	}
}

void ICanvas::Line( int xBegin, int xEnd, int yBegin, int yEnd )
{
	HDC hDC = GetPaintingDC();
	if(hDC)
	{
		::MoveToEx(hDC, xBegin, yBegin, NULL);
		::LineTo(hDC, xEnd, yEnd);
	}
}

void ICanvas::Rectangle( int left, int top, int right, int bottom )
{
	HDC hDC = GetPaintingDC();
	if(hDC)
	{
		::Rectangle(hDC, left, top, right, bottom);
	}
}

void ICanvas::EnableMouseScroll( BOOL bEnabled )
{
	m_bMouseScrollEnabled = bEnabled;
}

void ICanvas::OnMouseEvent( int nEvent, int x, int y, int nFlags, int nDelta )
{
	if(nEvent == DUEVENT_LBUTTONDOWN)
	{
		m_MousePoint = Point(x, y);
	}
	else if(nEvent == DUEVENT_MOUSEMOVE)
	{
		if (nFlags == DUEVENT_FLAG_LBUTTON)
		{
			Point pt(x, y);
			m_ZoomEngine.SetXScrollValue(m_ZoomEngine.GetXScrollValue() + m_MousePoint.x - pt.x);
			m_ZoomEngine.SetYScrollValue(m_ZoomEngine.GetYScrollValue() + m_MousePoint.y - pt.y);

			m_MousePoint = pt;
		}
	}
	else if(nEvent == DUEVENT_LBUTTONUP)
	{
		Point pt(x, y);
		m_ZoomEngine.SetXScrollValue(m_ZoomEngine.GetXScrollValue() + m_MousePoint.x - pt.x);
		m_ZoomEngine.SetYScrollValue(m_ZoomEngine.GetYScrollValue() + m_MousePoint.y - pt.y);
	}
	else if(nEvent == DUEVENT_MOUSEWHEEL)
	{
		if(m_bMouseScrollEnabled)
		{
			if (nDelta > 0)
			{
				m_ZoomEngine.Zoom(0.2);
			}
			else
			{
				m_ZoomEngine.Zoom(-0.2);
			}
		}
	}
}

void ICanvas::GetCanvasPaintRect( int* pX, int *pY, int* pWidth, int* pHeight )
{
	if(m_hWnd)
	{
		RECT clientRect;
		::GetClientRect(m_hWnd, &clientRect);
		m_ZoomEngine.SetWindowSize(Size(clientRect.right - clientRect.left, clientRect.bottom - clientRect.top));
	}

	Rect r = m_ZoomEngine.GetCanvasPaintRect();
	if(pX) *pX = (int)r.x;
	if(pY) *pY = (int)r.y;
	if(pWidth) *pWidth = (int)r.width;
	if(pHeight) *pHeight = (int)r.height;
}

void ICanvas::GetWindowPaintRect( int* pX, int *pY, int* pWidth, int* pHeight )
{
	if(m_hWnd)
	{
		RECT clientRect;
		::GetClientRect(m_hWnd, &clientRect);
		m_ZoomEngine.SetWindowSize(Size(clientRect.right - clientRect.left, clientRect.bottom - clientRect.top));
	}

	Rect r = m_ZoomEngine.GetWindowPaintRect();
	if(pX) *pX = (int)r.x;
	if(pY) *pY = (int)r.y;
	if(pWidth) *pWidth = (int)r.width;
	if(pHeight) *pHeight = (int)r.height;
}

void ICanvas::Init()
{
	m_hWnd = NULL;
	m_nPixelFormat = DUPIX_BGR;
	m_nWidth = 0;
	m_nHeight = 0;
	m_nStride = 0;
	m_nDrawMode = DUDRAW_NORMAL;
	m_hDC = NULL;
	m_hOldPen = NULL;
	m_bMouseScrollEnabled = TRUE;
}

void ICanvas::Zoom( double fScale )
{
	m_ZoomEngine.Zoom(fScale);
}

void ICanvas::SetZoomValue( double fValue )
{
	m_ZoomEngine.SetZoomValue(fValue);
}

double ICanvas::GetZoomValue()
{
	return m_ZoomEngine.GetZoomValue();
}

void ICanvas::WindowPointToCanvas( int wndX, int wndY, int* pCanvX, int* pCanvY )
{
	Point p = m_ZoomEngine.WindowPointToCanvas(Point(wndX, wndY));
	if(pCanvX) *pCanvX = (int)p.x;
	if(pCanvY) *pCanvY = (int)p.y;
}

void ICanvas::WindowPointToInsideCanvas( int wndX, int wndY, int* pCanvX, int* pCanvY )
{
	Point p = m_ZoomEngine.WindowPointToInsideCanvas(Point(wndX, wndY));
	if(pCanvX) *pCanvX = (int)p.x;
	if(pCanvY) *pCanvY = (int)p.y;
}

void ICanvas::CanvasPointToWindow( int canvX, int canvY, int* pWndX, int* pWndY )
{
	Point p = m_ZoomEngine.CanvasPointToWindow(Point(canvX, canvY));
	if(pWndX) *pWndX = (int)p.x;
	if(pWndY) *pWndY = (int)p.y;
}

DRAWUTIL_NS_END
