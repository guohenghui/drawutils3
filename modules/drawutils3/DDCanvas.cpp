#include "stdafx.h"
#include "DDCanvas.h"
#include "DXSurfaceMgr.h"

#pragma comment(lib, "ddraw.lib")
#pragma comment(lib, "dxguid.lib")

#include <algorithm>

DRAWUTIL_NS_BEGIN

#define DXFAIL_BREAK(FUN) \
	if(FAILED(hr)) { \
		LogError(FUN " DXError: 0x%X", (int)hr); \
		break;\
	}

int CDDCanvas::s_IdCounter = 0;

CDDCanvas::CDDCanvas(void)
{
	Init();

	m_Id = s_IdCounter++;
}

CDDCanvas::~CDDCanvas(void)
{
	Release();
}

void CDDCanvas::Init()
{
	ICanvas::Init();

	lpDD = NULL;
	lpDDSPrimary = NULL;
	lpDDSOffScr = NULL;

	m_FrameCounter = 0;
}

bool CDDCanvas::Create( HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode )
{
	LogDebug("CDDCanvas::Create: hWnd: %lld, PixelFormat: %d, W: %d, H: %d, Stride: %d, DrawMode: %d", (__int64)hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode);

	HRESULT hr = S_OK;

	do{
		// 创建DirectCraw对象
		hr = DirectDrawCreateEx(NULL,(VOID**)&lpDD,IID_IDirectDraw7,NULL); 
		// if(FAILED(hr)) break;
		DXFAIL_BREAK("DirectDrawCreateEx");

		// 设置协作层
		hr = lpDD->SetCooperativeLevel(/*_hWnd*/ 0, DDSCL_NORMAL /*| DDSCL_FULLSCREEN| DDSCL_NOWINDOWCHANGES*/);
		DXFAIL_BREAK("SetCooperativeLevel");

		// 创建主表面
		DDSURFACEDESC2 ddsd;
		memset(&ddsd,0,sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);  
		ddsd.dwFlags = DDSD_CAPS;//| DDSD_BACKBUFFERCOUNT  
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;//| DDSCAPS_FLIP | DDSCAPS_COMPLEX;//DDSCAPS_PRIMARYSURFACE;  
		hr = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);  
		DXFAIL_BREAK("CreateSurface");

		// 创建剪切板 
		LPDIRECTDRAWCLIPPER pClipper = NULL; 
		hr = lpDD->CreateClipper(NULL, &pClipper, NULL);
		DXFAIL_BREAK("CreateClipper");
		hr = pClipper->SetHWnd(NULL, hWnd);
		DXFAIL_BREAK("SetHWnd");
		hr = lpDDSPrimary->SetClipper(pClipper); 
		DXFAIL_BREAK("SetClipper");

		// 创建离屏表面对象
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_VIDEOMEMORY;//| DDSCAPS_OVERLAY | DDSCAPS_OFFSCREENPLAIN;
		ddsd.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
		ddsd.dwWidth = nWidth;
		ddsd.dwHeight = nHeight;
		ddsd.ddpfPixelFormat = GetPixelFormat(nPixelFormat); 

		hr = lpDD->CreateSurface(&ddsd, &lpDDSOffScr, NULL);
		DXFAIL_BREAK("CreateSurface");
	} while (false);

	if(FAILED(hr))
	{
		Release();
		
		return false;
	}

	bool bRet = ICanvas::Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode);

	LogDebug("CDDCanvas::Create End");
	return bRet;
}

void CDDCanvas::Release()
{
	LogDebug("CDDCanvas::Release ...");

	if(lpDDSOffScr) lpDDSOffScr->Release(); 
	if(lpDDSPrimary) lpDDSPrimary->Release(); 
	if(lpDD) lpDD->Release(); 

	Init();

	LogDebug("CDDCanvas::Release End");
}


DDPIXELFORMAT CDDCanvas::GetPixelFormat( int nPixelFormat)
{
	DDPIXELFORMAT pixelFormats[] = 
	{
		{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 32, 0x00FF0000,0x0000FF00,0x000000FF, 0}, // DUPIX_BGR
		{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 32, 0x00FF0000,0x0000FF00,0x000000FF, 0}, // DUPIX_BGRA
		{sizeof(DDPIXELFORMAT), DDPF_FOURCC,MAKEFOURCC('Y','V','1','2'),0,0,0,0,0},    // DUPIX_I420
		{sizeof(DDPIXELFORMAT), DDPF_FOURCC,MAKEFOURCC('Y','V','1','2'),0,0,0,0,0},    // DUPIX_YV12
	};

	return pixelFormats[nPixelFormat]; 
} 

void CDDCanvas::BeginPaint(BYTE* pData)
{
	DUImage img;
	ZeroMemory(&img,sizeof(img));
	img.nPlaneCount = 1;
	img.pDatas[0] = pData;
	img.nStrides[0] = m_nStride;
	
	BeginPaint(&img);
}

void CDDCanvas::BeginPaint(DUImage* pImage)
{
	HRESULT hr = DD_OK; 

	if(!lpDDSOffScr)
		throw Exception(ERR_NO_CANVAS, 0);

	do
	{
		m_FrameCounter++;
	
		DDSURFACEDESC2 ddsd;
		ZeroMemory(&ddsd,sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);

		// 		DDBLTFX bltfx; memset(&bltfx, 0,sizeof(DDBLTFX)); bltfx.dwSize = sizeof(DDBLTFX);
		// 		// convert the rgb given into what ever pixel format we may have
		// 		bltfx.dwFillColor = rgbconverter_->Convert(GetRValue(BackFillColor),
		// 			GetGValue(BackFillColor),
		// 			GetBValue(BackFillColor));
		// 		HRESULT hr = surface->Blt(NULL, NULL, NULL, DDBLT_COLORFILL|DDBLT_WAIT,&bltfx);

		hr = lpDDSOffScr->Lock(NULL, &ddsd, DDLOCK_WAIT | DDLOCK_WRITEONLY, NULL); //锁定页面
		DXFAIL_BREAK("Lock");

		if(m_FrameCounter % (25 * 60) == 0)
		{
			LogDebug("[ID: %d]: Frame: %lld, FillData", m_Id, m_FrameCounter);
		}

		FillData(*pImage,ddsd);

		hr = lpDDSOffScr->Unlock(NULL);
		DXFAIL_BREAK("Unlock");

		//if(pDC != NULL)
		//{
		//	// Draw
		//	HRESULT hr2 = lpDDSOffScr->GetDC(&m_hDC); // Will Lock
		//	if(pDC != NULL) *pDC = m_hDC;
		//}
	}
	while(false);

	if(FAILED(hr))
		throw Exception(ERR_NO_CANVAS, 0);
}

void CDDCanvas::EndPaint()
{
	HRESULT hr = DD_OK; 

	if(!lpDDSOffScr)
		throw Exception(ERR_NO_CANVAS, 0);

	bool bltFailed = false;

	do
	{
		//if(m_hDC != NULL)
		//{
		//	if(m_hOldPen != NULL)
		//	{
		//		HPEN hPen = (HPEN)::SelectObject(m_hDC, m_hOldPen);
		//		::DeleteObject(hPen);
		//	}
		//
		//	hr = lpDDSOffScr->ReleaseDC(m_hDC);
		//	m_hDC = NULL;
		//}
		
		// BLT显示
		XRect drawRect;
		GetClientRect(m_hWnd, &drawRect);
		// Do padding
		drawRect.left += m_Padding.left;
		drawRect.top += m_Padding.top;
		drawRect.right -= m_Padding.right;
		drawRect.bottom -= m_Padding.bottom;
		if(drawRect.right <= drawRect.left || drawRect.bottom <= drawRect.top)
			break;

		ClientToScreen(m_hWnd, (POINT*)&drawRect);
		ClientToScreen(m_hWnd, (POINT*)&drawRect + 1);

		HRESULT hr = S_OK;
		int width = m_nWidth;
		int height = m_nHeight;
		if(m_nDrawMode == DUDRAW_NORMAL)
		{
			int w = std::min(width, drawRect.Width());
			int h = std::min(height, drawRect.Height());
			if(w >= 0 && height >= 0)
			{
				XRect srcRect = XRect(0, 0, w, h);
				XRect dstRect = XRect(drawRect.left, drawRect.top, drawRect.left + w, drawRect.top + h);
				hr = lpDDSPrimary->Blt(&dstRect, lpDDSOffScr, &srcRect, DDBLT_WAIT, NULL);
				if(FAILED(hr)) bltFailed = true;
				DXFAIL_BREAK("DUDRAW_NORMAL Blt");
			}
		}
		else if(m_nDrawMode == DUDRAW_CENTER)
		{
			int w = std::min(width, drawRect.Width());
			int h = std::min(height, drawRect.Height());
			if(w >= 0 && height >= 0)
			{
				XRect srcRect = XRect(width / 2 - w / 2, height / 2 - h / 2, width / 2 + w / 2, height / 2 + h / 2);
				POINT cenPoint = {(drawRect.left + drawRect.right) / 2, (drawRect.top + drawRect.bottom) / 2 };
				XRect dstRect = XRect(cenPoint.x - w / 2, cenPoint.y - h / 2, cenPoint.x + w / 2, cenPoint.y + h / 2);
				hr = lpDDSPrimary->Blt(&dstRect, lpDDSOffScr, &srcRect, DDBLT_WAIT, NULL);
				DXFAIL_BREAK("Blt");
			}
		}
		else if(m_nDrawMode == DUDRAW_STRECH)
		{
			XRect srcRect(m_ROI);
			NormalizeROI(srcRect);

			if(srcRect.IsRectEmpty())
			{
				hr = lpDDSPrimary->Blt(&drawRect, lpDDSOffScr, NULL, DDBLT_WAIT, NULL);
			}
			else
			{
				hr = lpDDSPrimary->Blt(&drawRect, lpDDSOffScr, &srcRect, DDBLT_WAIT, NULL);
			}

			if(FAILED(hr)) bltFailed = true;
			DXFAIL_BREAK("DUDRAW_STRECH Blt");
		}
		else if(m_nDrawMode == DUDRAW_ZOOM)
		{
			XRect srcRect(m_ROI);
			NormalizeROI(srcRect);

			if(srcRect.IsRectEmpty())
			{
				GetZoomedInnerWindow(&drawRect.left,&drawRect.top,&drawRect.right,&drawRect.bottom,(LONG)width,(LONG)height);
				hr = lpDDSPrimary->Blt(&drawRect, lpDDSOffScr, NULL, DDBLT_WAIT, NULL);
			}
			else
			{
				GetZoomedInnerWindow(&drawRect.left,&drawRect.top,&drawRect.right,&drawRect.bottom,(LONG)srcRect.Width(),(LONG)srcRect.Height());
				hr = lpDDSPrimary->Blt(&drawRect, lpDDSOffScr, &srcRect, DDBLT_WAIT, NULL);
			}

			if(FAILED(hr)) bltFailed = true;
			DXFAIL_BREAK("DUDRAW_ZOOM Blt");
		}
		else if(m_nDrawMode == DUDRAW_AUTO)
		{
			int l, t, w, h;
			GetCanvasPaintRect(&l, &t, &w, &h);
			XRect srcRect(l, t, l + w, t + h);
			GetWindowPaintRect(&l, &t, &w, &h);
			XRect dstRect(l, t, l + w, t + h);
			ClientToScreen(m_hWnd, (POINT*)&dstRect);
			ClientToScreen(m_hWnd, (POINT*)&dstRect + 1);

			hr = lpDDSPrimary->Blt(&dstRect, lpDDSOffScr, &srcRect, DDBLT_WAIT, NULL);
			if(FAILED(hr)) bltFailed = true;
			DXFAIL_BREAK("DUDRAW_AUTO Blt");
		}
	}
	while(false);

	hr = lpDD->RestoreAllSurfaces(); 
	ICanvas::EndPaint();

	// 处理错误
	if(bltFailed)
	{
		throw Exception(ERR_DRAW_FAILED, 0);
	}
}

void CDDCanvas::NormalizeROI( XRect& r )
{
	if(r.left < 0) r.left = 0;
	if(r.top < 0) r.top = 0;
	if(r.right < 0) r.right = 0;
	if(r.bottom < 0) r.bottom = 0;

	if(r.left > m_nWidth) r.left = m_nWidth;
	if(r.top > m_nHeight) r.top = m_nHeight;
	if(r.right > m_nWidth) r.right = m_nWidth;
	if(r.bottom > m_nHeight) r.bottom = m_nHeight;
}

struct _RGB24
{
	unsigned char r,g,b;
};
struct _RGB32
{
	unsigned char r,g,b,a;
};

#define __COPY_RGB(_RGB_FORMAT)							\
for(DWORD i=0; i<ddsd.dwHeight; i++)					\
{														\
	int widthStepSrc = 0;								\
	int widthStepDst = 0;								\
	for (DWORD j=0;j<ddsd.dwWidth;++j)					\
	{													\
		*((_RGB_FORMAT*)(lpSurf+widthStepDst))			\
			=*((_RGB_FORMAT*)(pData+widthStepSrc));		\
		widthStepSrc += sampleStepX;					\
		widthStepDst += 4;								\
	}													\
	pData+=(srcStride);								\
	lpSurf+=ddsd.lPitch;								\
}

#define DRAW_TOP            0
#define DRAW_LEFT           0

void CDDCanvas::FillData( DUImage& img, DDSURFACEDESC2& ddsd )
{
	LPBYTE lpSurf = (LPBYTE)ddsd.lpSurface;
	int width = m_nWidth;
	int height = m_nHeight;
	int step = m_nStride;
	int imageType = m_nPixelFormat;

	if(imageType == DUPIX_I420 || imageType == DUPIX_YV12)
	{ 
		int halfHeight = height / 2;
		//填充 
		LPBYTE lpY = NULL, lpV = NULL, lpU = NULL;
		int YStep = 0, UStep = 0, VStep = 0;

		if(img.nPlaneCount == 3)
		{
			lpY = img.pDatas[0];
			YStep = img.nStrides[0];

			if(imageType == DUPIX_I420)
			{
				lpU = img.pDatas[1];
				lpV = img.pDatas[2];

				UStep = img.nStrides[1];
				VStep = img.nStrides[2];
			}
			else
			{

				lpV = img.pDatas[1];
				lpU = img.pDatas[2];

				VStep = img.nStrides[1];
				UStep = img.nStrides[2];
			}
		}
		else
		{
			BYTE* pData = img.pDatas[0];
			lpY = pData;
			lpV = pData + width * height;
			lpU = pData + width * height * 5 / 4;

			if(imageType == DUPIX_I420)
			{
				lpU = pData + width * height;
				lpV = pData + width * height * 5 / 4;
			}

			YStep = width;
			UStep = YStep / 2;
			VStep = YStep / 2;
		}
		
		//LPBYTE lpSurf = (LPBYTE)ddsd.lpSurface;
		LPBYTE lpY1 = lpSurf;
		LPBYTE lpV1 = lpSurf + ddsd.lPitch * ddsd.dwHeight;
		LPBYTE lpU1 = lpV1 + ddsd.lPitch  * ddsd.dwHeight / 4;
		int nOffset = DRAW_TOP * width + DRAW_LEFT;
		//填充离屏表面
		if(lpSurf)
		{
			int i = 0, j = 0;

			int YStep1 = ddsd.lPitch;
			int UVStep1 = YStep1 / 2;

			// fill Y data
			lpY += nOffset;
			// #pragma omp parallel for
			for( i = 0; i < height; i++)
			{
				memcpy(lpY1, lpY, YStep);
				lpY += YStep;
				lpY1 += YStep1;
			}

			// fill V data
			lpV += DRAW_TOP * width / 4 + DRAW_LEFT / 2;
			// #pragma omp parallel for
			for( i = 0; i < halfHeight; i++)
			{
				memcpy(lpV1, lpV, /*VStep*/width / 2);
				lpV += VStep;
				lpV1 += UVStep1;
			}

			// fill U data
			lpU += DRAW_TOP * width / 4 + DRAW_LEFT / 2;
			// #pragma omp parallel for
			for( i = 0; i < halfHeight; i++)
			{
				memcpy(lpU1, lpU, /*UStep*/width / 2);
				lpU += UStep;
				lpU1 += UVStep1;
			}  
		}
	}
	else if(imageType == DUPIX_BGR || imageType == DUPIX_BGRA)
	{ 
		BYTE* pData = img.pDatas[0];

		int srcPixelSize = ((imageType == DUPIX_BGR) ? 3 : 4); 
		int dstPixelSize = 4;
		int srcStride = step;
		int dstStride = ddsd.lPitch;
		int W = ddsd.dwWidth;
		int H = ddsd.dwHeight;
		if(imageType == DUPIX_BGR)
		{
			// #pragma omp parallel for
			for(int i = 0; i < H; i++)
			{
				LPBYTE pSrc = pData + i * srcStride;
				LPBYTE pDst = lpSurf + i * dstStride;

				for (int j = 0; j < W; j++)
				{
					pDst[0] = pSrc[0];
					pDst[1] = pSrc[1];
					pDst[2] = pSrc[2];

					pSrc += srcPixelSize;
					pDst += dstPixelSize;
				}
			}
		}
		else if(imageType == DUPIX_BGRA)
		{
			// #pragma omp parallel for
			for(int i = 0; i < H; i++)
			{
				LPBYTE pSrc = pData + i * srcStride;
				LPBYTE pDst = lpSurf + i * dstStride;

				for (int j = 0; j < W; j++)
				{
					pDst[0] = pSrc[0];
					pDst[1] = pSrc[1];
					pDst[2] = pSrc[2];
					pDst[3] = pSrc[3];

					pSrc += srcPixelSize;
					pDst += dstPixelSize;
				}
			}
		}

		else
		{
			//__COPY_RGB(_RGB32);
		}
	}
}

DRAWUTIL_NS_END