#pragma once

#include <vector>

#include "icanvas.h"
#include "DXBOX.h"
#include "DXSurfaceMgr.h"

DRAWUTIL_NS_BEGIN

class CD3DCanvas :
	public ICanvas
{
public:
	CD3DCanvas(void);
	~CD3DCanvas(void);
protected:
	void Init(); 
public:
	bool Create(HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode);
	void Release();
	void BeginPaint(BYTE* pData);
	void BeginPaint(DUImage* pImage);
	void EndPaint();
private:
	
private:
	__int64 m_FrameCounter;

	dx::IVideoPainter* m_VP;
private:
	void NormalizeROI(XRect& r);

	dx::PixelFormat MapPixelFormat(int format);
	std::vector<uint8_t> m_Img;
	int canvasWidth;
	int canvasHeight;
private:
	CD3DCanvas(CD3DCanvas&);
	CD3DCanvas& operator=(CD3DCanvas&);
};

DRAWUTIL_NS_END