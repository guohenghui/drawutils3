#pragma once
#include <exception>
#include "ZoomEngine.h"

DRAWUTIL_NS_BEGIN

enum ErrorCode
{
	ERR_OK,
	ERR_DX,
	ERR_NO_CANVAS,
	ERR_DRAW_FAILED,
};

class Exception : public std::exception
{
public:
	Exception(ErrorCode errCode)
	{
		m_ErrCode = errCode;
		m_HR = S_OK;
	}

	Exception(ErrorCode errCode, HRESULT hr)
	{
		m_ErrCode = errCode;
		m_HR = hr;
	}

private:
	ErrorCode m_ErrCode;
	HRESULT m_HR;
};

void Throw(ErrorCode errCode);
void Throw(ErrorCode errCode, HRESULT hr);

#define DUCHECK_THROW(errCode, hr) if(FAILED(hr)) Throw(errCode, hr);

class ICanvas
{
public:
	ICanvas(void);
	virtual ~ICanvas(void);

	virtual bool Create(HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode) = 0;
	virtual void Release() = 0;

	virtual void SetDrawMode(int nDrawMode);
	virtual void SetPadding(int nLeft, int nTop, int nRight, int nBottom);
	virtual void SetROI(int nLeft, int nTop, int nRight, int nBottom);

	virtual void BeginPaint(BYTE* pData) = 0;
	virtual void BeginPaint(DUImage* pImage) = 0;
	virtual void EndPaint() = 0;
	virtual void Refresh();
	virtual void SetPen(int nSyle, int nWidth, COLORREF Clr);

	virtual void MoveTo(int x, int y);
	virtual void LineTo(int x, int y);
	virtual void Line(int xBegin, int xEnd, int yBegin, int yEnd);
	virtual void Rectangle(int left, int top, int right, int bottom);

	virtual void EnableMouseScroll( BOOL bEnabled );
	virtual void OnMouseEvent(int nEvent, int x, int y, int nFlags, int nDelta);
	virtual void GetCanvasPaintRect(int* pX, int *pY, int* pWidth, int* pHeight);
	virtual void GetWindowPaintRect(int* pX, int *pY, int* pWidth, int* pHeight);
	virtual void Zoom(double fScale);
	virtual void SetZoomValue(double fValue);
	virtual double GetZoomValue();

	virtual void WindowPointToCanvas( int wndX, int wndY, int* pCanvX, int* pCanvY );
	virtual void WindowPointToInsideCanvas( int wndX, int wndY, int* pCanvX, int* pCanvY );
	virtual void CanvasPointToWindow( int canvX, int canvY, int* pWndX, int* pWndY );
protected:
	virtual void Init();
	virtual HDC GetPaintingDC();
protected:
	HWND m_hWnd;
	int m_nPixelFormat;
	int m_nWidth;
	int m_nHeight;
	int m_nStride;
	int m_nDrawMode;
	HDC m_hDC;
	HPEN m_hOldPen;
	BOOL m_bMouseScrollEnabled;
	RECT m_Padding;
	RECT m_ROI;
private:
	typedef ZoomEngine::Point_d Point;
	typedef ZoomEngine::Rect_d Rect;
	typedef ZoomEngine::Size_d Size;

	ZoomEngine m_ZoomEngine;
	Point m_MousePoint;
};

DRAWUTIL_NS_END