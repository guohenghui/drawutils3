#include "stdafx.h"
#include "D3DCanvas.h"
#include "DXSurfaceMgr.h"

DRAWUTIL_NS_BEGIN

CD3DCanvas::CD3DCanvas(void)
{
	Init();

	m_VP = NULL;
}

CD3DCanvas::~CD3DCanvas(void)
{
	Release();
}

void CD3DCanvas::Init()
{
	ICanvas::Init();

	m_FrameCounter = 0;
	m_nPixelFormat = 0;
}

dx::PixelFormat du::CD3DCanvas::MapPixelFormat(int format)
{
	switch(format)
	{
	case DUPIX_BGR:
	case DUPIX_BGRA: 
		return dx::PIXEL_ARGB;
	case DUPIX_I420:
	case DUPIX_YV12:
		return dx::PIXEL_YV12;
	}

	throw std::exception("δ֪��ɫ��ʽ��");
}

bool CD3DCanvas::Create( HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode )
{
	LogDebug("CD3DCanvas::Create ... ");

	ICanvas::Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode);

	bool bRet = false;
	if(DXCreateVideoPainter(0, &m_VP) == DXERR_OK)
	{
		m_nPixelFormat = nPixelFormat;
		dx::PixelFormat fmt = MapPixelFormat(nPixelFormat);
		
		m_VP->Open(hWnd, dx::SurfaceInfo(nWidth, nHeight, fmt));
		bRet = true;
	}
	else
	{
		LogError("DXCreateVideoPainter Failed: %s", DXGetErrorMessage(DXGetLastError()));
	}

	LogDebug("CD3DCanvas::Create End");
	return bRet;
}

void CD3DCanvas::Release()
{
	LogDebug("CD3DCanvas::Release ...");

	if(m_VP != NULL)
	{
		DXDeleteVideoPainter(m_VP);
		m_VP = NULL;
	}

	Init();

	LogDebug("CD3DCanvas::Release End");
}

void CD3DCanvas::BeginPaint(BYTE* pData)
{
	DUImage img;
	ZeroMemory(&img,sizeof(img));
	img.nPlaneCount = 1;
	img.pDatas[0] = pData;
	img.nStrides[0] = m_nStride;
	
	BeginPaint(&img);
}

void CD3DCanvas::BeginPaint(DUImage* pImage)
{
	if(m_VP == NULL || !m_VP->IsOpened()) throw Exception(ERR_NO_CANVAS, 0);
	m_FrameCounter++;

	canvasWidth = canvasHeight = 0;
	m_VP->BeginPaint(&canvasWidth, &canvasHeight);

	switch(m_nPixelFormat)
	{
	case DUPIX_BGR:
		{
			int srcPixelSize = 3; 
			int dstPixelSize = 4;
			int srcStride = pImage->nStrides[0];
			int dstStride = m_nWidth * 4;
			int W = m_nWidth;
			int H = m_nHeight;
	
			m_Img.resize(dstStride * H);

			uint8_t* pDstBuffer = m_Img.data();
			for(int i = 0; i < H; i++)
			{
				LPBYTE pSrc = pImage->pDatas[0] + i * srcStride;
				LPBYTE pDst = pDstBuffer + i * dstStride;

				for (int j = 0; j < W; j++)
				{
					pDst[0] = pSrc[0];
					pDst[1] = pSrc[1];
					pDst[2] = pSrc[2];
					pDst[3] = 255;

					pSrc += srcPixelSize;
					pDst += dstPixelSize;
				}
			}

			dx::LockedRect lr;
			if(m_VP->LockVideoImage(lr))
			{
			 	DXCopyImage(dx::PIXEL_ARGB, m_nWidth, m_nHeight, m_Img.data(), lr.Data, dstStride, lr.Stride);

				m_VP->UnlockVideoImage();
			}
		}
		break;
	case DUPIX_BGRA: 
		{
			dx::LockedRect lr;
			if(m_VP->LockVideoImage(lr))
			{
				DXCopyImage(dx::PIXEL_ARGB, m_nWidth, m_nHeight, pImage->pDatas[0], lr.Data, pImage->nStrides[0], lr.Stride);

				m_VP->UnlockVideoImage();
			}
		}
		break;
	case DUPIX_I420:
		{
			dx::LockedRect lr;
			if(m_VP->LockVideoImage(lr))
			{
				if(pImage->pDatas[1] != NULL && pImage->pDatas[2] != NULL)
				{
					DXCopyYUV420Image2(m_nWidth, m_nHeight, pImage->pDatas[0], pImage->pDatas[2], pImage->pDatas[1], lr.Data, pImage->nStrides[0], pImage->nStrides[1], lr.Stride, lr.Stride / 2);
				}
				else
				{
					DXCopyYUV420Image(m_nWidth, m_nHeight, pImage->pDatas[0], lr.Data, pImage->nStrides[0], pImage->nStrides[0] / 2, lr.Stride, lr.Stride / 2, true);
				}

				m_VP->UnlockVideoImage();
			}
		}
		break;
	case DUPIX_YV12:
		{
			dx::LockedRect lr;
			if(m_VP->LockVideoImage(lr))
			{
				if(pImage->pDatas[1] != NULL && pImage->pDatas[2] != NULL)
				{
					DXCopyYUV420Image2(m_nWidth, m_nHeight, pImage->pDatas[0], pImage->pDatas[1], pImage->pDatas[2], lr.Data, pImage->nStrides[0], pImage->nStrides[1], lr.Stride, lr.Stride / 2);
				}
				else
				{
					DXCopyYUV420Image(m_nWidth, m_nHeight, pImage->pDatas[0], lr.Data, pImage->nStrides[0], pImage->nStrides[0] / 2, lr.Stride, lr.Stride / 2, false);
				}

				m_VP->UnlockVideoImage();
			}
		}
		break;
	}
}

void CD3DCanvas::EndPaint()
{
	if(m_VP == NULL || !m_VP->IsOpened()) throw Exception(ERR_NO_CANVAS, 0);

	m_VP->BeginScene();
	do
	{
		XRect drawRect(0, 0, canvasWidth, canvasHeight);
		// Do padding
		drawRect.left += m_Padding.left;
		drawRect.top += m_Padding.top;
		drawRect.right -= m_Padding.right;
		drawRect.bottom -= m_Padding.bottom;
		if(drawRect.right <= drawRect.left || drawRect.bottom <= drawRect.top)
			break;

		int width = m_nWidth;
		int height = m_nHeight;
		if(m_nDrawMode == DUDRAW_NORMAL)
		{
			int w = std::min(width, drawRect.Width());
			int h = std::min(height, drawRect.Height());
			if(w >= 0 && height >= 0)
			{
				dx::Rect srcRect = dx::Rect(0, 0, w, h);
				dx::Rect dstRect = dx::Rect(drawRect.left, drawRect.top, w, h);
				m_VP->DrawVideoImage(srcRect, dstRect);
			}
		}
		else if(m_nDrawMode == DUDRAW_CENTER)
		{
			int w = std::min(width, drawRect.Width());
			int h = std::min(height, drawRect.Height());
			if(w >= 0 && height >= 0)
			{
				XRect srcRect = XRect(width / 2 - w / 2, height / 2 - h / 2, width / 2 + w / 2, height / 2 + h / 2);
				POINT cenPoint = {(drawRect.left + drawRect.right) / 2, (drawRect.top + drawRect.bottom) / 2 };
				XRect dstRect = XRect(cenPoint.x - w / 2, cenPoint.y - h / 2, cenPoint.x + w / 2, cenPoint.y + h / 2);
				
				dx::Rect srcRect2 = dx::Rect(srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height());
				dx::Rect dstRect2 = dx::Rect(dstRect.left, dstRect.top, dstRect.Width(), dstRect.Height());
				m_VP->DrawVideoImage(srcRect2, dstRect2);
			}
		}
		else if(m_nDrawMode == DUDRAW_STRECH)
		{
			XRect srcRect(m_ROI);
			NormalizeROI(srcRect);

			if(srcRect.IsRectEmpty())
			{
				dx::Rect srcRect2 = dx::Rect(0, 0, m_nWidth, m_nHeight);
				dx::Rect dstRect2 = dx::Rect(drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height());
				m_VP->DrawVideoImage(srcRect2, dstRect2);
			}
			else
			{
				dx::Rect srcRect2 = dx::Rect(srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height());
				dx::Rect dstRect2 = dx::Rect(drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height());
				m_VP->DrawVideoImage(srcRect2, dstRect2);
			}
		}
		else if(m_nDrawMode == DUDRAW_ZOOM)
		{
			XRect srcRect(m_ROI);
			NormalizeROI(srcRect);

			if(srcRect.IsRectEmpty())
			{
				GetZoomedInnerWindow(&drawRect.left,&drawRect.top,&drawRect.right,&drawRect.bottom,(LONG)width,(LONG)height);
	
				dx::Rect srcRect2 = dx::Rect(0, 0, m_nWidth, m_nHeight);
				dx::Rect dstRect2 = dx::Rect(drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height());
				m_VP->DrawVideoImage(srcRect2, dstRect2);
			}
			else
			{
				GetZoomedInnerWindow(&drawRect.left,&drawRect.top,&drawRect.right,&drawRect.bottom,(LONG)srcRect.Width(),(LONG)srcRect.Height());
			
				dx::Rect srcRect2 = dx::Rect(srcRect.left, srcRect.top, srcRect.Width(), srcRect.Height());
				dx::Rect dstRect2 = dx::Rect(drawRect.left, drawRect.top, drawRect.Width(), drawRect.Height());
				m_VP->DrawVideoImage(srcRect2, dstRect2);
			}
		}
	}
	while(false);
	m_VP->EndScene();
	m_VP->EndPaint();

	ICanvas::EndPaint();
}

void CD3DCanvas::NormalizeROI( XRect& r )
{
	if(r.left < 0) r.left = 0;
	if(r.top < 0) r.top = 0;
	if(r.right < 0) r.right = 0;
	if(r.bottom < 0) r.bottom = 0;

	if(r.left > m_nWidth) r.left = m_nWidth;
	if(r.top > m_nHeight) r.top = m_nHeight;
	if(r.right > m_nWidth) r.right = m_nWidth;
	if(r.bottom > m_nHeight) r.bottom = m_nHeight;
}

DRAWUTIL_NS_END