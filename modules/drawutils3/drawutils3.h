/**
Copyright (c) 2024 Henghui Guo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#if !defined(DRAWUTILS3_H)
#define DRAWUTILS3_H

#if defined(_WIN32) || defined(WIN32)
#define DUCALL __stdcall
#else
#define DUCALL
#endif // defined(_WIN32) || defined(WIN32)

#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
 
	// CanvasType，画布类型
#define DU_CANVAS_AUTO         -1   // 自适应选择 第一次尝试: DU_CANVAS_DD
#define DU_CANVAS_GDI          0    // GDI画布
#define DU_CANVAS_DD           1    // DirectDraw画布
#define DU_CANVAS_D3D          2    // D3D画布

	// PixelFormat，像素格式
#define DUPIX_BGR              0
#define DUPIX_BGRA             1
#define DUPIX_I420             2
#define DUPIX_YV12             3

	// DrawMode， 绘图模式
#define DUDRAW_NORMAL          0    // 图像被置于 PictureBox 的左上角。如果图像比包含它的 PictureBox 大， 则该图像将被剪裁掉。
#define DUDRAW_CENTER          1    // 如果 PictureBox 比图像大，则图像将居中显示。 如果图像比 PictureBox 大，则图片将居于 PictureBox 中心，而外边缘将被剪裁掉。
#define DUDRAW_STRECH          2    // PictureBox 中的图像被拉伸或收缩，以适合 PictureBox 的大小。
#define DUDRAW_ZOOM            3    // 图像大小按其原有的大小比例被增加或减小。 
#define DUDRAW_AUTO            4    // 自动模式。 

	// 鼠标消息
#define DUEVENT_MOUSEMOVE          0
#define DUEVENT_LBUTTONDOWN        1
#define DUEVENT_RBUTTONDOWN        2
#define DUEVENT_MBUTTONDOWN        3
#define DUEVENT_LBUTTONUP          4
#define DUEVENT_RBUTTONUP          5
#define DUEVENT_MBUTTONUP          6
#define DUEVENT_LBUTTONDBLCLK      7
#define DUEVENT_RBUTTONDBLCLK      8
#define DUEVENT_MBUTTONDBLCLK      9
#define DUEVENT_MOUSEWHEEL         10

	// 鼠标消息标志
#define DUEVENT_FLAG_LBUTTON       1
#define DUEVENT_FLAG_RBUTTON       2
#define DUEVENT_FLAG_MBUTTON       4
#define DUEVENT_FLAG_CTRLKEY       8
#define DUEVENT_FLAG_SHIFTKEY      16
#define DUEVENT_FLAG_ALTKEY        32

	// 图像定义
	typedef struct _DUImage
	{
		int nPlaneCount;

		BYTE* pDatas[4];
		int nStrides[4];
	} DUImage;

	void* DUCALL DUCreateCanvas(int nCanvasType, HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode);
	BOOL DUCALL DUDeleteCanvas(void* pCanvas);

	BOOL DUCALL DUSetDrawMode(void* pCanvas, int nDrawMode);
	BOOL DUCALL DUSetPadding(void* pCanvas, int nLeft, int nTop, int nRight, int nBottom);

	BOOL DUCALL DUSetROI(void* pCanvas, int nLeft, int nTop, int nRight, int nBottom);

	//BOOL DUCALL DUBeginPaint(void* pCanvas, BYTE* pData);
	BOOL DUCALL DUBeginPaint2(void* pCanvas, DUImage* pImage);
	BOOL DUCALL DUEndPaint(void* pCanvas);
	BOOL DUCALL DURefresh(void* pCanvas);

	//BOOL DUCALL DUSetPen(void* pCanvas, int nSyle, int nWidth, COLORREF Clr);

	//BOOL DUCALL DUMoveTo(void* pCanvas, int x, int y);
	//BOOL DUCALL DULineTo(void* pCanvas, int x, int y);
	//BOOL DUCALL DULine(void* pCanvas, int xBegin, int yBegin, int xEnd, int yEnd);
	//BOOL DUCALL DURectangle(void* pCanvas, int left, int top, int right, int bottom);

	//BOOL DUCALL DUEnableMouseScroll(void* pCanvas, BOOL bEnabled);
	//BOOL DUCALL DUOnMouseEvent(void* pCanvas, int nEvent, int x, int y, int nFlags, int nDelta);

	// 功能：      图像缩放，
	//             只有在DUDRAW_AUTO（自动模式）有效
	// 参数：
	//   pCanvas： 画布句柄
	//   nScale：  缩放数值 
	//             缩放比例 = nScale/%
	// 返回值：    0： 失败， 1： 成功
	//BOOL DUCALL DUZoom(void* pCanvas, int nScale);

	//BOOL DUCALL DUSetZoomValue(void* pCanvas, int nValue);
	//BOOL DUCALL DUGetZoomValue(void* pCanvas, int* pValue);

	//BOOL DUCALL DUWindowPointToCanvas(void* pCanvas, int wndX, int wndY, int* pCanvX, int* pCanvY);
	//BOOL DUCALL DUWindowPointToInsideCanvas(void* pCanvas, int wndX, int wndY, int* pCanvX, int* pCanvY);
	//BOOL DUCALL DUCanvasPointToWindow(void* pCanvas, int canvX, int canvY, int* pWndX, int* pWndY);

#ifdef __cplusplus
};
#endif // __cplusplus
#endif // DRAWUTILS3_H