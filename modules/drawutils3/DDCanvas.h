#pragma once

#include "icanvas.h"
#include "ddraw.h" 
#include "DXSurfaceMgr.h"

DRAWUTIL_NS_BEGIN

class CDDCanvas :
	public ICanvas
{
public:
	CDDCanvas(void);
	~CDDCanvas(void);
protected:
	void Init(); 
public:
	bool Create(HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode);
	void Release();
	void BeginPaint(BYTE* pData);
	void BeginPaint(DUImage* pImage);
	void EndPaint();
private:
	LPDIRECTDRAW7 lpDD;                  // DirectDraw 对象指针
	LPDIRECTDRAWSURFACE7 lpDDSPrimary;   // DirectDraw 主表面指针
	LPDIRECTDRAWSURFACE7 lpDDSOffScr;    // DirectDraw 离屏表面指针

	DDPIXELFORMAT pixelFormat;
private:
	DDPIXELFORMAT GetPixelFormat(int nPixelFormat);
	void FillData( DUImage& img, DDSURFACEDESC2& ddsd );

	__int64 m_FrameCounter;
	static int s_IdCounter;
	int m_Id;

private:
	void NormalizeROI(XRect& r);
};

DRAWUTIL_NS_END