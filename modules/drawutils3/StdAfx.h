// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__74CF04E7_BA47_4390_BDB6_C9E2AEC5966D__INCLUDED_)
#define AFX_STDAFX_H__74CF04E7_BA47_4390_BDB6_C9E2AEC5966D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// Insert your headers here
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>

#undef min
#undef max

#include <algorithm>

#include "drawutils3.h"

#include "mediacore.h"

#define DRAWUTIL_NS_BEGIN namespace du{
#define DRAWUTIL_NS_END };
#define USING_DRAWUTIL_NS using namespace du;

#define LogDebug printf
#define LogInfo printf
#define LogError printf

#endif // !defined(AFX_STDAFX_H__74CF04E7_BA47_4390_BDB6_C9E2AEC5966D__INCLUDED_)
