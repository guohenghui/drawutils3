// drawutil.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "DXSurfaceMgr.h"
#include <Windows.h>
#include "DDCanvas.h"
#include "GDICanvas.h"
#include "D3DCanvas.h"

USING_DRAWUTIL_NS

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

void* DUCALL DUCreateCanvas(int nCanvasType, HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode)
{
	ICanvas* pCan = NULL;

	if(nCanvasType == DU_CANVAS_AUTO || nCanvasType == DU_CANVAS_D3D)
	{
		pCan = new CD3DCanvas();

		if(!pCan->Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode))
		{
			delete pCan;
			pCan = NULL;
		}
	}

	if(nCanvasType == DU_CANVAS_AUTO || nCanvasType == DU_CANVAS_DD)
	{
		pCan = new CDDCanvas();

		if(!pCan->Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode))
		{
			delete pCan;
			pCan = NULL;
		}
	}

	if(pCan == NULL)
	{
		if(nCanvasType == DU_CANVAS_AUTO || nCanvasType == DU_CANVAS_GDI)
		{
			pCan = new CGDICanvas();

			if(!pCan->Create(hWnd, nPixelFormat, nWidth, nHeight, nStride, nDrawMode))
			{
				delete pCan;
				pCan = NULL;
			}
		}
	}

	return pCan;
}

BOOL DUCALL DUDeleteCanvas( void* pCanvas )
{
	if(pCanvas == NULL) return FALSE;
	
	ICanvas* pCan = (ICanvas*)pCanvas;
	delete pCan;

	return TRUE;
}

BOOL DUCALL DUSetDrawMode( void* pCanvas, int nDrawMode )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->SetDrawMode(nDrawMode);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUSetPadding( void* pCanvas, int nLeft, int nTop, int nRight, int nBottom )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->SetPadding(nLeft, nTop, nRight, nBottom);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUSetROI( void* pCanvas, int nLeft, int nTop, int nRight, int nBottom )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->SetROI(nLeft, nTop, nRight, nBottom);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUBeginPaint(void* pCanvas, BYTE* pData)
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->BeginPaint(pData);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUBeginPaint2(void* pCanvas, DUImage* pImage)
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->BeginPaint(pImage);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUEndPaint( void* pCanvas )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->EndPaint();
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DURefresh( void* pCanvas )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->Refresh();
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUSetPen( void* pCanvas, int nSyle, int nWidth, COLORREF Clr )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->SetPen(nSyle, nWidth, Clr);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUMoveTo( void* pCanvas, int x, int y )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->MoveTo(x, y);
	}
	catch(...)
	{
		return FALSE;
	}
	
	return FALSE;
}

BOOL DUCALL DULineTo( void* pCanvas, int x, int y )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->LineTo(x, y);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DULine( void* pCanvas, int xBegin, int yBegin, int xEnd, int yEnd )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->Line(xBegin, xEnd, yBegin, yEnd);
	}
	catch(...)
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL DUCALL DURectangle( void* pCanvas, int left, int top, int right, int bottom)
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->Rectangle(left, top, right, bottom);
	}
	catch(...)
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL DUCALL DUEnableMouseScroll( void* pCanvas, BOOL bEnabled )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->EnableMouseScroll(bEnabled);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUOnMouseEvent( void* pCanvas, int nEvent, int x, int y, int nFlags, int nDelta )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->OnMouseEvent(nEvent, x, y, nFlags, nDelta);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUZoom( void* pCanvas, int nScale )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->Zoom((double)nScale / 100);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUSetZoomValue( void* pCanvas, int nValue )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->SetZoomValue((double)nValue / 100);
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUGetZoomValue( void* pCanvas, int* pValue )
{
	if(pCanvas == NULL) return FALSE;
	if(pValue == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		*pValue = pCan->GetZoomValue() * 100;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUWindowPointToCanvas( void* pCanvas, int wndX, int wndY, int* pCanvX, int* pCanvY )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->WindowPointToCanvas(wndX, wndY, pCanvX, pCanvY);;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUWindowPointToInsideCanvas( void* pCanvas, int wndX, int wndY, int* pCanvX, int* pCanvY )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->WindowPointToInsideCanvas(wndX, wndY, pCanvX, pCanvY);;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DUCALL DUCanvasPointToWindow( void* pCanvas, int canvX, int canvY, int* pWndX, int* pWndY )
{
	if(pCanvas == NULL) return FALSE;

	ICanvas* pCan = (ICanvas*)pCanvas;

	try
	{
		pCan->CanvasPointToWindow(canvX, canvY, pWndX, pWndY);;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}
