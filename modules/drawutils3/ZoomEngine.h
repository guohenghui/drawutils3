﻿#pragma once

class ZoomEngine
{
public:
	struct Point_d
	{
		double x;
		double y;

		Point_d()
		{
			x = y = 0;
		}

		Point_d(double x, double y)
		{
			this->x = x;
			this->y = y;
		}
	};

	struct Size_d
	{
		double width;
		double height;

		Size_d()
		{
			width = height = 0;
		}

		Size_d(double width, double height)
		{
			this->width = width;
			this->height = height;
		}
	};

	struct Rect_d
	{
		double x;
		double y;
		double width;
		double height;

		Rect_d()
		{
			x = y = width = height = 0;
		}

		Rect_d(double x, double y, double width, double height)
		{
			this->x = x;
			this->y = y;
			this->width = width;
			this->height = height;
		}

		double Left()
		{
			return x;
		}

		double Right()
		{
			return x + width;
		}

		double Top()
		{
			return y;
		}

		double Bottom()
		{
			return y + height;
		}

		bool Contains(double x, double y)
		{
			return x >= Left() && x < Right() && y >= Top() && y < Bottom();
		}
	};

#define MIN_ZOOM_SCALE  0.01
#define MAX_ZOOM_SCALE  10000.0

	Size_d m_CanvasSize;
	Size_d m_WindowSize;
	Point_d m_ViewCenterPoint;

	// 0: Center
	// 1: Left-Top
	int m_CanvasOrientation;

	double m_ZoomScale;

	double m_XScrollValue;
	double m_YScrollValue;

	ZoomEngine()
	{
		Initialize();
	}

	void Initialize()
	{
		m_ZoomScale = 1;
		m_CanvasOrientation = 0;

		m_XScrollValue = 0;
		m_YScrollValue = 0;
	}

	void SetCanvasSize(Size_d size)
	{
		m_CanvasSize = size;
	}

	void SetWindowSize(Size_d size)
	{
		m_WindowSize = size;
	}

	void SetViewCenter(Point_d viewCenter)
	{
		m_ViewCenterPoint = viewCenter;
	}

	Point_d WindowPointToCanvas(Point_d pt)
	{
		Point_d pt2;

		Rect_d r = GetWindowPaintRect();
		if (GetXScrollLength() > 0)
		{
			pt2.x = GetXScrollValue() + pt.x;
		}
		else
		{
			pt2.x = pt.x - r.Left();
		}

		if (GetYScrollLength() > 0)
		{
			pt2.y = GetYScrollValue() + pt.y;
		}
		else
		{
			pt2.y = pt.y - r.Top();
		}

		pt2.x /= m_ZoomScale;
		pt2.y /= m_ZoomScale;

		return pt2;
	}

	Point_d WindowPointToInsideCanvas(Point_d pt)
	{
		Point_d pt2 = WindowPointToCanvas(pt);

		if (pt2.x < 0) pt2.x = 0;
		if (pt2.y < 0) pt2.y = 0;
		if (pt2.x > m_CanvasSize.width) pt2.x = m_CanvasSize.width;
		if (pt2.y > m_CanvasSize.height) pt2.y = m_CanvasSize.height;

		return pt2;
	}

	Point_d CanvasPointToWindow(Point_d pt)
	{
		Point_d pt2;

		Point_d ptZoom = Point_d(pt.x * m_ZoomScale, pt.y * m_ZoomScale);

		Rect_d r = GetWindowPaintRect();
		if (GetXScrollLength() > 0)
		{
			pt2.x = ptZoom.x - GetXScrollValue();
		}
		else
		{
			pt2.x = ptZoom.x + r.Left();
		}

		if (GetYScrollLength() > 0)
		{
			pt2.y = ptZoom.y - GetYScrollValue();
		}
		else
		{
			pt2.y = ptZoom.y + r.Top();
		}

		return pt2;
	}

	void SetZoomValue(double scale)
	{
		// 获得Scroll值
		double dX = GetXScrollLength();
		double dY = GetYScrollLength();
		m_ZoomScale = scale;

		if (m_ZoomScale < MIN_ZOOM_SCALE) m_ZoomScale = MIN_ZOOM_SCALE;
		if (m_ZoomScale > MAX_ZOOM_SCALE) m_ZoomScale = MAX_ZOOM_SCALE;

		// 设置Scroll值
		double l = GetXScrollLength();
		if (l > 0)
		{
			m_XScrollValue += (l - dX) / 2;
		}
		else
		{
			m_XScrollValue = 0;
		}

		if (m_XScrollValue < 0) m_XScrollValue = 0;
		if (m_XScrollValue > l) m_XScrollValue = l;

		l = GetYScrollLength();
		if (l > 0)
		{
			m_YScrollValue += (l - dY) / 2;
		}
		else
		{
			m_YScrollValue = 0;
		}

		if (m_YScrollValue < 0) m_YScrollValue = 0;
		if (m_YScrollValue > l) m_YScrollValue = l;
	}

	double GetZoomValue()
	{
		return m_ZoomScale;
	}

	void Zoom(double scale)
	{
		SetZoomValue(m_ZoomScale + m_ZoomScale * scale);
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="sizeMode">
	/// 0: 画布原始大小
	/// 1: 画布适应窗口大小
	/// </param>
	void SetCanvasViewSize(int sizeMode)
	{
		if (sizeMode == 0)
		{
			SetZoomValue(1.0f);
		}
		else if (sizeMode == 1)
		{
			if (m_CanvasSize.width * m_CanvasSize.height > 0)
			{
				double dW = m_WindowSize.width / m_CanvasSize.width;
				double dH = m_WindowSize.height / m_CanvasSize.height;

				double d = dW > dH ? dH : dW;

				SetZoomValue(d);
			}
		}
	}

	Size_d GetCanvasViewSize()
	{
		return Size_d(m_CanvasSize.width * m_ZoomScale, m_CanvasSize.height * m_ZoomScale);
	}

	double GetXScrollLength()
	{
		Size_d canvasViewSize = GetCanvasViewSize();

		double l = canvasViewSize.width - m_WindowSize.width;

		return l > 0 ? l : 0;
	}

	double GetYScrollLength()
	{
		Size_d canvasViewSize = GetCanvasViewSize();

		double l = canvasViewSize.height - m_WindowSize.height;

		return l > 0 ? l : 0;
	}

	double GetXScrollValue()
	{
		return m_XScrollValue;
	}

	double GetYScrollValue()
	{
		return m_YScrollValue;
	}

	void SetXScrollValue(double scrollValue)
	{
		if (scrollValue < 0) scrollValue = 0;
		if (scrollValue > GetXScrollLength()) scrollValue = GetXScrollLength();

		m_XScrollValue = scrollValue;
	}

	void SetYScrollValue(double scrollValue)
	{
		if (scrollValue < 0) scrollValue = 0;
		if (scrollValue > GetYScrollLength()) scrollValue = GetYScrollLength();

		m_YScrollValue = scrollValue;
	}

	Rect_d GetWindowPaintRect()
	{
		Size_d canvasViewSize = GetCanvasViewSize();

		Rect_d r;
		if (canvasViewSize.width >= m_WindowSize.width)
		{
			r.x = 0;
			r.width = m_WindowSize.width;
		}
		else
		{
			if (m_CanvasOrientation == 0)
			{
				r.x = m_WindowSize.width / 2 - canvasViewSize.width / 2;
				r.width = canvasViewSize.width;
			}
			else// if (m_CanvasOrientation == 1)
			{
				r.x = m_WindowSize.width / 2 - canvasViewSize.width / 2;
				r.width = canvasViewSize.width;
			}
		}

		if (canvasViewSize.height >= m_WindowSize.height)
		{
			r.y = 0;
			r.height = m_WindowSize.height;
		}
		else
		{
			if (m_CanvasOrientation == 0)
			{
				r.y = m_WindowSize.height / 2 - canvasViewSize.height / 2;
				r.height = canvasViewSize.height;
			}
			else// if (m_CanvasOrientation == 1)
			{
				r.y = m_WindowSize.height / 2 - canvasViewSize.height / 2;
				r.height = canvasViewSize.height;
			}
		}

		return r;
	}

	Rect_d GetCanvasPaintRect()
	{
		// 在Zoom图像中找画布绘图区域
		Size_d canvasViewSize = GetCanvasViewSize();

		Rect_d r;
		if (GetXScrollLength() > 0)
		{
			r.x = m_XScrollValue;
			r.width = m_WindowSize.width;
		}
		else
		{
			r.x = 0;
			r.width = canvasViewSize.width;
		}

		if (GetYScrollLength() > 0)
		{
			r.y = m_YScrollValue;
			r.height = m_WindowSize.height;
		}
		else
		{
			r.y = 0;
			r.height = canvasViewSize.height;
		}

		// 映射回源图像
		r.x /= m_ZoomScale;
		r.y /= m_ZoomScale;
		r.width /= m_ZoomScale;
		r.height /= m_ZoomScale;

		return r;
	}
};