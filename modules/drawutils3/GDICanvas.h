#pragma once

#include "icanvas.h"
#include "ddraw.h" 

DRAWUTIL_NS_BEGIN

class CGDICanvas :
	public ICanvas
{
public:
	CGDICanvas(void);
	~CGDICanvas(void);
private:
	void Init();
	void Reset();
public:
	bool Create(HWND hWnd, int nPixelFormat, int nWidth, int nHeight, int nStride, int nDrawMode);
	void Release();
	void BeginPaint(BYTE* pData);
	void BeginPaint(DUImage* pImage);
	void EndPaint();
	void Refresh();
private:
	HDC m_hFrontDC;
	HBITMAP m_hOldBmp;
	HBITMAP m_hBmp;
	BYTE* m_pData;
	bool m_bCreated;
private:
	void FillData( BYTE* pData );
	void fillBmpInfo(BITMAPINFO* pBMI, int width, int height, int bpp, int origin);
};

DRAWUTIL_NS_END