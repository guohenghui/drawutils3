
if "%GENERATOR%" == "" set GENERATOR=Visual Studio 17 2022

:: Build
if not exist build mkdir build
pushd build

call :BuildArch Win32
call :BuildArch x64

:: Pack
xcopy /CHSY ..\nuget\* nuget\
xcopy /CHY ..\LICENSE nuget\

nuget pack nuget

:PackEnd
popd & exit /B %ERRORLEVEL%

:BuildArch
    set buildDir=build-%~1
    cmake -G "%GENERATOR%" -S .. -B "%buildDir%" -A %~1 -DCMAKE_INSTALL_PREFIX=nuget/build/native

    cmake --build "%buildDir%" --config Debug
    IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed & GOTO :PackEnd
    cmake --build "%buildDir%" --config Release
    IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed & GOTO :PackEnd

    cmake --install "%buildDir%"
    IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed & GOTO :PackEnd

exit /B %ERRORLEVEL%